TL;DR Follow the [Git-Forking Workflow](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)

1. Fork this repository to your user namespace
2. Clone it

    `git clone https://gitlab.com/<username>/ddr.git`

3. Add another remote (i.e `upstream`) for the original upstream repository

    `git remote add upstream https://gitlab.com/pablo.fontanilla/ddr.git`

4. Pull the <upstream>/main branch into your origin/main branch

    `git pull upstream main`

5. Create a new branch to work in the new changes

    `git checkout -b dev_my_changes`

6. Push the new branch to your `origin` repository

    `git push origin dev_my_changes`

7. Go to gitlab and create an MR to the pablo.fontanilla original repository

Note: If you want to add new content, check the README for how to create and test new entries. Once your partida.MD file(s) are ready, just wait for me to merge the MR, deployment will be handled automatically by Gitlab CI
