---
title: "¿Qué es esto?"
date: 2023-02-19T16:06:00+01:00
featured_image: ''
omit_header_text: true
description: about page
type: page
menu: main

---


Esto es una página hecha con [Hugo](https://gohugo.io) para exponer las partidas que os puedo dirigir a vosotros, mis queridísimos jugadores.

## ¿Cómo lo uso?

Explora las partidas disponibles, y cuando veas una que te gusta, actualiza tu respuesta en el Google Form enlazado en la descripción para indicar que quieres jugarla.
Cuando se alcance el número mínimo de jugadores, avisaré de que va a organizarse la partida por si se quiere apuntar alguien más, abriendo un doodle para ver cuándo quedamos.

Si tienes cualquier duda, consulta la [FAQ](/faq/)
