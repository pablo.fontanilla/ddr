---
title: Preguntas frecuentes
featured_image: ''
omit_header_text: true
description: FAQ you
type: page
menu: main

---
# DDR UNOFFICIAL FAQ #

- **Se llama "Domingos de rol", ¿Es obligatorio que sea en domingo?**

No! Se juega cuando se pongan de acuerdo jugadores y DM. El nombre es solo marketing!

- **¿Me puedo apuntar a más de una partida? ¡Ya no me apetece jugar esta otra!**

Claro que sí, cuantas más mejor! Puedes editar tu respuesta en cualquier momento para añadir o quitar partidas que te apetece jugar

- **No conozco el sistema de la partida X. ¿Puedo apuntarme aun así?**

No hay que saber absolutamente nada de nada para apuntarse a ninguna partida. ***Guíate solo por lo que te guste*** tras leer la descripción. Por favor, que no os desanime "ser novatos", o "es que voy a molestar". Todos sois bienvenidos en todas las partidas!

- **La partida que quería jugar se ha llenado. ¿Qué hago?**

Tener un poquito de paciencia, en cuanto se llene un grupo, crearé una nueva instancia de la partida y podrás apuntarte a esa.

- **Me apetece dirigir una partida concreta, ¿puedo añadirla?**

Por supuesto, mándame un formulario o abre una MR! Por ahora las que he subido es porque quiero dirigirlas, pero cualquiera es bienvenido a dirigir. En el futuro podrás añadirla tú misma.

- **¿Dónde jugamos?**

Nuestra casa siempre está disponible, pero podemos jugar en cualquier sitio en el que se pongan de acuerdo los participantes.

- **¿Pero podemos jugar por internet?**

Claro, si todos los jugadores se ponen de acuerdo :)

- **No me entero cuando subes nuevas partidas, ¿qué puedo hacer?**

Tienes una feed RSS de todas las partidas en https://ddr.pablofontanilla.com/partidas/index.html para que puedas usarla en tu agregador favorito.

- **¿Por qué no simplemente organizas las partidas según te apetece?**

Porque me apetece dirigir todas las que pongo y quiero ver cómo funciona esto

- **¿Por qué no haces la partida X del sistema Y?**

Probablemente, no se me ha ocurrido. Envíame la información, y la añado. Vuestros deseos son órdenes para mi!

- **¿Por qué hay tantos juegos raros o de flipados?**

Porque me gusta probar cosas nuevas

- **¿Por qué tu página es tan mierda?**

Porque todavía no se lo que hago. Estamos trabajando en ello!

- **Qué rollo coger las respuestas, hacer el doodle...**

Sin duda, antes o después lo automatizaré :)

- **Si no me gusta el formulario de contacto, ¿hay otra manera de sugerir cosas?**

Por supuesto! Puedes abrir un issue en el proyecto: https://gitlab.com/pablo.fontanilla/ddr/

- **¿Vas a preparar pizzas cuando vayamos?**

Probablemente no, a menos que no sea la primera vez que dirijo esa partida :)

- **Ese logo no lo has hecho tú!**

Cierto, lo ha hecho logo.com

- **¿Por qué a veces pones símbolos de apertura (¿¡) y a veces no?**

Por estética, según lo formal que sea dónde va la frase lo pongo o no

- **No me creo que muchas de estas preguntas sean frecuentes**

Yo me las hago muchas veces, eso también cuenta...

---

***Si no has encontrado la respuesta a tus preguntas, ve al formulario de contacto y dispara!***
