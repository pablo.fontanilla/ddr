---
title: "Sirvientes de la Reina de Ceniza"
date: 2023-02-19T20:28:56+01:00
draft: false
tags: ["pbta", "mazmorreo", "colaborativo"]
---
## ¿Qué secretos yacen bajo el monasterio en ruinas?

Breve introducción de la partida

- **Sistema**: Dungeonworld
- **Tipo partida**: Escrita, narrativa
- **Sesiones**: One-shot
- **Jugadores**: 3-5
- **Link**: https://dungeon-world.com/


![La oscuridad](/images/cinder-queen.png)



Érase una vez un monasterio, encaramado en la loma de una montaña llamada el Cuerno de Hvitr. Los monjes que en él vivían se encomendaban a Hvitr, dios de las tormentas y la justicia, y custodiaban textos arcanos y artefactos sagrados. Pero sobre todo, eran guardianes, porque el Cuerno de Hvitr era un volcán activo, y por tanto una puerta a Ellorash, el plano elemental del fuego.

El fin de esta hermandad llegó con una erupción de la que salió la Hueste Ardiente de Ellorash, una legión de otro mundo liderada por la semidios Gildarthe. La leyenda que Hvitry bajó de los cielos para sellar la apertura planar y rechazar a Gildarthe, pero no antes de que el monasterio y todos los que en él se hallaban murieran ahogadas en una inundación de lava.

300 años después, empiezan a ocurrir sucesos inexplicables. El poblado de Meervold, al pie del Cuerno de Hvitr pasa por momentos oscuros. Extrañas luces se ven en las frías lomas. Aullidos distantes resuenan en la noche. El Concilio de Anciones se ha quebrado, y los que no han huído de Meervold, solo permanecen porque el miedo les paraliza.

¿Qué secretos yacen bajo el monasterio en ruinas? ¿Permanece con vida alguno de los aldeandos desaparecidos? ¿Hay algo de verdad en los rumores de artefactos sagrados enterrados bajo la montaña?

Solo un idiota o un aventurero intentaría encontrar las respuestas.

Una aventura para Dungeon World, diseñada para ser fácil de usar y conducir a la improvisación.
Si esto te suena bien, apúntate en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)


