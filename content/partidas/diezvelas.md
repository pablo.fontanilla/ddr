---
title: "Ten Candles"
date: 2023-02-19T20:28:15+01:00
draft: false
tags: ["tragedia", "colaborativo", "ciencia-ficción", "one-shot"]
---
## Horror trágico a la luz de las velas

Ten Candles es un juego de rol narrativo y sin preparación, diseñado para partidas de horror trágico de una sesión. Se juega a la luz de diez velas de té, que proporcionan la atmósfera, sirven como reloj del juego y en última instancia, como fuego.

<!--more-->

- **Sistema**: Ten Candles
- **Tipo partida**: improvisada, colaborativa
- **Sesiones**: One-shot
- **Jugadores**: 3-5
- **Link**: [Aquí](https://cavalrygames.com/ten-candles-info)

Ten Candles suele ser descrito como "horror trágico", más que "horror de supervivencia (survival horror) por una razón: En Ten Candles no hay supervivientes. Al final de la partida, todos los jugadores que queden vivos morirán. En este sentido, Ten Candles no es un juego sobre "ganar", o derrotar a los monstruos. Es un juego sobre lo que ocurre en la oscuridad, y a aquellos que intenta sobrevivir en ella. Es un juego sobre llegar al límite de la desesperación y la cordura, buscando la esperanza en un mundo sin ella, e intentando hacer algo de valor con tus últimas horas.

Si a pesar de lo que has leído, o quizá ***por*** lo que has leído te apetece probarlo, apúntate en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)

