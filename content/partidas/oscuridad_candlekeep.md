---
title: "Una oscuridad profunda e inquietante"
date: 2023-02-19T20:29:57+01:00
draft: false
tags: ["dnd5", "mazmorreo", "terror", "one-shot"]
---
## Sostienes en tus manos un delgado libro encuadernado en cuero negro... 

Habéis llegado a Candlekeep, la legendaria biblioteca entre planos. ¿Qué misterios encontraréis en ella?

- **Sistema**: Dungeons and Dragons 5
- **Tipo partida**: Escrita
- **Sesiones**: One-shot
- **Jugadores**: 3-5
- **Link**: https://dnd.wizards.com/

"Una oscuridad profunda e inquietante" llegó a Candlekeep con otro grupo de aventureros, que lo recibieron de un aldeano que lo encontró entre las posesiones de su difunto abuelo. Este, un bardo itinerante, decía haberlo escrito. Aunque parece no ser más que una combinación de diario y folletín de terror, cumple los requisitos de ser una obra única. Habla de la lenta muerte de un pueblecito de montaña llamado Vermeillón. Después de un terrible accidente en la mina de platino, los supervivientes y otros aldeanos empezaron a desaparecer...

Una aventura para Dungeons and Dragons ambientada en la legendaria biblioteca de Candlekeep!
Si esto te suena bien, apúntate en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)


![La oscuridad](/images/darkness.png)

