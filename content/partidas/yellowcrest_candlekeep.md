---
title: "Yellowcrest_candlekeep"
date: 2023-02-19T20:29:42+01:00
draft: true
---
## Titulo partida (o sistema si no hay partida)

Breve introducción de la partida

- **Sistema**: Dungeons and Dragons 5
- **Tipo partida**: Escrita
- **Sesiones**: One-shot
- **Jugadores**: 3-5
- **Link**: Link-al-sistema
- **Etiquetas**: las que quieras!

Parrafo descriptivo de la partida



