---
title: "El tomo cefíreo de Zikan"
date: 2023-02-19T20:29:57+01:00
draft: false
tags: ["dnd5", "mazmorreo", "one-shot"]
---
## Sostienes en tus manos un grueso tomo con la portada de mármol en cuero azul... 

Habéis llegado a Candlekeep, la legendaria biblioteca entre planos. ¿Qué misterios encontraréis en ella?

- **Sistema**: Dungeons and Dragons 5
- **Tipo partida**: Escrita
- **Sesiones**: One-shot
- **Jugadores**: 3-5
- **Link**: https://dnd.wizards.com/

El tomo cefíreo de Zikran fue donado a Candlekeep por aventureros que buscaban refugio de una tormenta que parecía seguirles donde quiera que fueran.
Incapaz de abrirlo, un bibliotecario con poca experiencia lo catalogó como un tratado en los planos interiores, basándose en la reputación de Zikran.
El libro ha acabado en vuestra posesión, y sentís una dulce brisa al sostenerlo en vuestras manos... 

Una aventura para Dungeons and Dragons ambientada en la legendaria biblioteca de Candlekeep!

![El tomo cefíreo](/images/zikran_tome.png)

Si esto te suena bien, apúntate en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)




