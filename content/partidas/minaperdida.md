---
title: "La Mina Perdida de Phandelver"
date: 2023-02-28T23:29:57+01:00
draft: false
tags: ["iniciación", "mazmorreo", "aventuras", "dnd5", "campaña"]
---
## ¿Qué está pasando en Phandalin? 

*Los personajes se encuentran en la ciudad de Neverwinter cuando su patrón y amigo, el enano Gundren Rockseeker, les contrata para escoltar un carromato hasta Phandalin.*

<!--more-->

- **Sistema**: Dungeons and Dragons 5
- **Tipo partida**: Escrita
- **Sesiones**: Mini-campaña, 8-10 sesiones
- **Jugadores**: 3-5
- **Link**: https://dnd.wizards.com/es

*Gundren se ha adelantado y ya ha partido hacia Phandalin. Acompañado de Sildar Hallwinter, necesita atender unos negocios antes de que los aventureros lleguen con los suministros. 
Pero las cosas en Phandalin no marchan bien, y los aventureros se enfrentarán a más de lo que esperaban...*

La Mina Perdida de Phandelver es una aventura para entre tres y cinco personajes de nivel 1. A lo largo de ella avanzarán hasta nivel 5. Esta aventura está ubicada cerca de la ciudad de Neverwinter, en la región de la Costa de la Espada de la ambientación Reinos Olvidados.

Es ideal para iniciarse en Dungeons and Dragons, así que si te apetece, apúntate en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)

![Hacia Phandalin!](/images/lmopcover.jpg)
