---
title: "Alice ha desaparecido"
date: 2023-02-19T20:29:26+01:00
draft: false
tags: ["contemporaneo", "realista", "improvisado", "colaborativo"]
---
## ¿Podrás encontrarla? 

Un juego de rol mudo, pero con Whatsapp, sobre encontrar a tu amiga desaparecida.

<!--more-->
![Alice](/images/alice_missing_poster.jpg)

- **Sistema**: Alicia ha desaparecido
- **Tipo partida**: Improvisada, colaborativa
- **Sesiones**: One-shot
- **Jugadores**: 2-4
- **Link**: [Aquí](https://www.huntersentertainment.com/alice-is-missing)

Alicia ha desaparecido es un juego de rol mudo sobre la desaparición de Alicia Briarwood, una estudiante de instituto del pequeño pueblo de Silent Falls.

Se juega en directo, pero sin comunicación verbal! Los jugadores serán su personaje durante los 90 minutos de partida (más 45 de creación de personajes y situación), pero en vez de hablar, se mandarán Whatsapps con el resto de jugadores, tanto en grupo como individualmente, como si no estuvieran en el mismo sitio.

Este juego trata temas adultos, que podemos filtrar y descartar al principio de la partida, pero tiende a sesiones emocionalmente intensas.
Si esto te suena bien, apúntate en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)




