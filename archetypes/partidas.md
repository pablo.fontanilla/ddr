---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
tags: sample_tag_1, sample_tag_2
---
## Subtitulo

Breve introducción de la partida

<!--more-->
![Imagen](/images/ruta_imagen.***)

- **Sistema**: Nombre del sistema
- **Tipo partida**: Escrita, improvisada, colaborativa
- **Sesiones**: One-shot, mini-campaña, campaña
- **Jugadores**: X-Y
- **Link**: Link-al-sistema
- **Etiquetas**: las que quieras!

Parrafo descriptivo de la partida

Si todo esto te suena bien, apúntate en el [formulario!](https://forms.gle/GwN7U5tXGuuPe2XW8)


