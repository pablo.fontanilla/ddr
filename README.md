# README

This is the repo for [ddr.pablofontanilla.com](https://ddr.pablofontanilla.com/)

Feel free to [contribute](./CONTRIBUTING.md)!

## How to start working with the site

- Have Hugo [installed](https://gohugo.io/installation/)
- Add the [ananke theme](https://github.com/theNewDynamic/gohugo-theme-ananke) as a git submodule
  - Inside the root folder: 

    `git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke`

## How to add a new entry
If you have already installed Hugo and ananke, you can proceed.

Hugo entries are based on [archetypes](https://gohugo.io/content-management/archetypes/), which are templates with prefilled values.

To create a new entry for a "partida", on the root folder:

`hugo new content/partidas/kids_on_brooms.md`

where "kids_on_brooms" is the desired file name.

and then edit the file according to the instructions inside, keeping "draft: true"

If you want to see how it looks:

`hugo serve -D`

Once you're happy with the result, if you're hosting a copy of this site yourself, build it with:

`hugo`

And if you're using [storage cloud hosting](https://gohugo.io/hosting-and-deployment/hugo-deploy/):

'hugo deploy'

If you are making a contribution to ddr.pablofontanilla.com, see the contributing guide for more details on how to create a MR
